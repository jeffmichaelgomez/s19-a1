let num = 2;
const getCube = num ** 3;
let message = `The cube of ${num} is ${getCube}`
console.log(message);

const fullAdd = ["258 Washington Ave NW", "California", "90011"];
const [street, state, postal] = fullAdd;
console.log(`I live at ${street}, ${state} ${postal}`);

const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	measure: "20 ft 3 in"
}
const {name,type,weight,measure} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measure}.`);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog('Frankie',5,'Miniature Dachshund');
console.log(myDog);